from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.utils.translation import gettext as _

def index(request):
    return HttpResponse(_("Hello World!"))


class HomePage(TemplateView):
    template_name = 'example/home.html'